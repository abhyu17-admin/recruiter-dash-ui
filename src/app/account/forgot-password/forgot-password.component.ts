import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpserviceService } from 'src/app/providers/httpservice.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotForm : FormGroup;
  constructor(private router: Router, public httpService: HttpserviceService) {
    this.forgotForm=new FormGroup({
    'email': new FormControl('', [Validators.email])
  });
 }
 submitforgot(){
   let self=this;
  this.httpService.postWithoutAuth("recruiters/forgot_password",this.forgotForm.value).subscribe((res:any)=>{
    if(res.type){
      self.httpService.showSuccess(res.message);
      self.router.navigateByUrl("/sign-in"); 
   }
     else {
       if(res.message) {
         self.httpService.showError(res.message);
      }
    }
  })

 }
 ngOnInit(): void {
 }
}
