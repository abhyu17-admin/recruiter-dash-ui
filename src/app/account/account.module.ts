import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { OnboardingDetailsComponent } from './onboarding-details/onboarding-details.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../@theme/material-module';
import { ThemeModule } from '../@theme/@theme.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegistrationSuccessMailComponent } from './registration-success-mail/registration-success-mail.component';
import { NewPasswordComponent } from './new-password/new-password.component';
import { OnboardingDetailComponent } from './onboarding-detail/onboarding-detail.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';


@NgModule({
  declarations: [
    AccountComponent,
    OnboardingDetailsComponent,
    ForgotPasswordComponent,
    RegistrationSuccessMailComponent,
    NewPasswordComponent,
    OnboardingDetailComponent,
    UpdateProfileComponent,
    ChangePasswordComponent

  ],
  imports: [
    ThemeModule,
    AccountRoutingModule
  ]
  
})
export class AccountModule { }
