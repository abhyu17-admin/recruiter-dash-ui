import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registration-success-mail',
  templateUrl: './registration-success-mail.component.html',
  styleUrls: ['./registration-success-mail.component.scss']
})
export class RegistrationSuccessMailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
