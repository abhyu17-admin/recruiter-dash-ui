import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveCertificateDialogComponent } from './save-certificate-dialog.component';

describe('SaveCertificateDialogComponent', () => {
  let component: SaveCertificateDialogComponent;
  let fixture: ComponentFixture<SaveCertificateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveCertificateDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveCertificateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
