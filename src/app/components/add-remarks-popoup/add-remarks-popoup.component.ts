import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpserviceService } from 'src/app/providers/httpservice.service';
declare var $:any;

@Component({
  selector: 'app-add-remarks-popoup',
  templateUrl: './add-remarks-popoup.component.html',
  styleUrls: ['./add-remarks-popoup.component.scss']
})
export class AddRemarksPopoupComponent implements OnInit {
  remarkForm: FormGroup;
  JobDetails: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public httpService: HttpserviceService) {
    console.log(data);
    this.remarkForm = new FormGroup({
      "_id": new FormControl(data.statusDetails._id, [Validators.required]),
      "remark": new FormControl(data.statusDetails.remark, [Validators.required])
    })
  }
  onSubmit() {
    let self = this;
    // console.log("API not implemented yet");

    this.httpService.postWithoutAuth("interviews/update_remark", this.remarkForm.value).subscribe((res: any) => {
      if (res.type) {
        self.httpService.showSuccess(res.message);
        $("#close-modal").click();
      }
      else {
        if (res.message) {
          this.httpService.showError(res.message);
        }
      }
    });
  }
  ngOnInit(): void {
  }

}
